/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.mouseOver;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class BrightSpark extends WebDriverBaseTestPage<WebDriverTestPage>

{
	public static JavascriptExecutor js;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }

	@QAFTestStep(description = "user clicks On MenuBar")
    public void userClicksOnMenuBar()
    {
		waitForNotVisible("login.loader.loc");
		waitForVisible("login.MenuBar.lnk", 30);
		mouseOver("login.MenuBar.lnk");
		click("login.MenuBar.lnk");
    }
	
	@QAFTestStep(description = "user click On menu {0}")
    public void userClickOnMenu(String MenuName) throws IOException
    {
		waitForNotVisible("login.loader.loc", 50);
		QAFExtendedWebElement menu = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("BrightSpark.RandR.lnk"), MenuName ));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", menu);
		
	  }
	

	@QAFTestStep(description = "user click on RRPatOnBackBar")
    public void userClickOnRRPatOnBackBar() throws IOException
    {
		waitForNotVisible("login.loader.loc", 50);
		QAFExtendedWebElement Nominate = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("BrightSpark.Nominate.lnk")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", Nominate);
		
		waitForNotVisible("login.loader.loc", 50);
		QAFExtendedWebElement PatOnBack = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("BrightSpark.PatOnBack.lnk")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", PatOnBack);
		
	  }
	
	@QAFTestStep(description = "user click On Select PatOnBackCardButton")
	public void userClickOnSelectPatOnBackCardButton()
	{		
		waitForNotVisible("login.loader.loc", 50);
		QAFExtendedWebElement SelectPatOnBackCardButton = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("BrightSpark.SelectPatOnBackCardButton.btn")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", SelectPatOnBackCardButton);
	}
	
	@QAFTestStep(description = "user Enter Key Contribution by Nominee {0}")
	public void userEnterKeyContributionByNominee(String ContributionKey)
	{
		waitForNotVisible("login.loader.loc", 50);
		waitForVisible("BrightSpark.EnterKeyContributionByNominee.txb");
		sendKeys(ContributionKey, "BrightSpark.EnterKeyContributionByNominee.txb");
	}
	
	@QAFTestStep(description = "user click on RRNaminate Bright Spark")
    public void userClickOnRRNaminateBrightSpark() throws IOException
    {
		
		waitForNotVisible("login.loader.loc", 50);
		QAFExtendedWebElement Nominate = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("BrightSpark.Nominate.lnk")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", Nominate);
		
		waitForNotVisible("login.loader.loc", 50);
		QAFExtendedWebElement BrightSparkTab = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("BrightSpark.BrightSparkTab.lnk")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", BrightSparkTab);
		
	  }
	
	
	@QAFTestStep(description = "user click on MyRRTab MyNominationTab")
		    public void userClickOnMyRRTabAndClickOnMyNominationTab()
		    {
				waitForNotVisible("login.loader.loc", 100);
				QAFExtendedWebElement MyRRTab = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("BrightSpark.MyRandR.lnk")));		
				((JavascriptExecutor)driver).executeScript("arguments[0].click();", MyRRTab);

				waitForNotVisible("login.loader.loc", 100);
				QAFExtendedWebElement MyNominationTab = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("BrightSpark.MyNominations.lnk")));		
				((JavascriptExecutor)driver).executeScript("arguments[0].click();", MyNominationTab);
				
		    }
	
	@QAFTestStep(description = "user selects SelectCardButton")
    public void userselectsSelectCardButton()
    {
		waitForNotVisible("login.loader.loc");
		waitForVisible("BrightSpark.SelectCardTab.btn");
		mouseOver("BrightSpark.SelectCardTab.btn");
		click("BrightSpark.SelectCardTab.btn");
    }
	
	@QAFTestStep(description = "user selects NomineeTab {0}")
    public void userSelectsNomineeTab(String NomineeName)
    {
		waitForNotVisible("login.loader.loc");
		waitForVisible("BrightSpark.SelectNominee.dpn");
		click("BrightSpark.SelectNominee.dpn");
		sendKeys(NomineeName, "BrightSpark.placeholder.txb");
		Actions action = new Actions(driver);
		action.sendKeys(Keys.RETURN).perform();

    }
	
	@QAFTestStep(description = "user selects ProjectTab {0}")
    public void userSelectsProjectTab(String ProjectName)
    {
		waitForNotVisible("login.loader.loc");
		waitForVisible("BrightSpark.SelectProject.dpn");
		click("BrightSpark.SelectProject.dpn");
		sendKeys(ProjectName, "BrightSpark.placeholder.txb");
		Actions action = new Actions(driver);
		action.sendKeys(Keys.RETURN).perform();
		
    }
	
	@QAFTestStep(description="user enters the Text in all required fields {0} {1} {2} {3}")
	public void userEntersTheTextInAllRequiredFields(String ChallengingSituationsFaced, String SolutionsProvidedTab, String BenefitsAccruedTab, String RationaleForNominationTab) {

		sendKeys(ChallengingSituationsFaced, "BrightSpark.ChallengingSituationsFaced.txb");
		sendKeys(SolutionsProvidedTab, "BrightSpark.SolutionsProvidedTab.txb");
		sendKeys(BenefitsAccruedTab, "BrightSpark.BenefitsAccruedTab.txb");
		sendKeys(RationaleForNominationTab, "BrightSpark.RationaleForNominationTab.txb");
		
	}
	
	@QAFTestStep(description = "user clicks on PostButton")
    public void userClicksOnPostButton()
    {
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("BrightSpark.Post.btn", 30);
		click("BrightSpark.Post.btn");
		waitForNotVisible("login.loader.loc", 100);
    }
	
	@QAFTestStep(description = "user click on Managermenu {0}")
    public void userClickOnManagermenu(String MgrmenuName)
    {
		waitForNotVisible("login.loader.loc", 100);
		QAFExtendedWebElement mgrmenu = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("BrightSpark.MgrRandR.lnk"), MgrmenuName));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", mgrmenu);
		
    }
	
	@QAFTestStep(description = "user click on RRRequests")
    public void userClickOnRRRequests(String RRequests)
    {
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("BrightSpark.RRRequests.lnk", 30);
		QAFExtendedWebElement mgrRRequests = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("BrightSpark.RRRequests.lnk"), RRequests));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", mgrRRequests);
		
    }
	@QAFTestStep(description = "user click on R and R requests Button")
	public void clickOnRRrequestsButton()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForNotVisible("BrightSpark.RRRequests.lnk", 30);
		QAFWebElement RRrequestsTab = driver.findElement("BrightSpark.RRRequests.lnk");
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", RRrequestsTab);
		
	}
	
	@QAFTestStep(description = "user verify BrightSpark Name")
	public void userVerifyBrightSparkName()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("BrightSpark.BrightSparkName.lnk", 100);
		verifyPresent("BrightSpark.BrightSparkName.lnk");
	}
	
	@QAFTestStep(description = "user click on BrightSparkTab where Mgr status pending")
	public void userCclickOnBrightSparkTabWhereMgrStatusPending()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("BrightSpark.ManagerStatusBR.lnk", 30);
		QAFWebElement BrightSparkPendingTab = driver.findElement("BrightSpark.ManagerStatusBR.lnk");
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", BrightSparkPendingTab);
		
	}
	
	@QAFTestStep(description = "user click on BrightSparkTab where HR status Rejected")
	public void userClickOnBrightSparkTabWhereHRStatusRejected()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("BrightSpark.HRStatusRejected.lnk", 30);
		QAFWebElement BrightSparkRejectTab = driver.findElement("BrightSpark.HRStatusRejected.lnk");
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", BrightSparkRejectTab);
	}
	
	
	@QAFTestStep(description = "user click on BrightSparkTab where Manager status Approved and HR status pending")
	public void userClickOnBrightSparkTabWhereManagerStatusApprovedAndHRStatusPending()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("BrightSpark.MgrStatusApdHRStatusPng.lnk", 30);
		QAFWebElement MgrStatusApdHRStatusPng = driver.findElement("BrightSpark.MgrStatusApdHRStatusPng.lnk");
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", MgrStatusApdHRStatusPng);
		
	}
	
	
	@QAFTestStep(description = "user click on BrightSparkTab where Manager status Approved and HR status Approved")
	public void userClickOnBrightSparkTabWhereManagerStatusApprovedAndHRStatusApproved()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("BrightSpark.MgrStatusApdHRStatusApd.lnk", 30);
		QAFWebElement MgrStatusApdHRStatusApd = driver.findElement("BrightSpark.MgrStatusApdHRStatusApd.lnk");
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", MgrStatusApdHRStatusApd);
		
	}

	@QAFTestStep(description = "user click on Reject Button")
	public void userClickOnRejectButton()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("common.Reject.btn");
		click("common.Reject.btn");
	}
		
	@QAFTestStep(description = "user pass the Rejection comments {0}")
	public void userPassTheRejectionComments(String RejectedComments)
	{
//		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("common.Reject.btn");
		sendKeys(RejectedComments, "BrightSpark.RejectCommentsTab.txb");
	}
	
	@QAFTestStep(description = "user click on Approve Button")
	public void userClickOnApproveButton()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("common.Approve.btn");
		click("common.Approve.btn");
	}
	
	@QAFTestStep(description = "user pass the Approve comments {0}")
	public void userPassTheApproveComments(String ApproveComments)
	{
//		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("BrightSpark.ApproveCommentsTab.txb");
		sendKeys(ApproveComments, "BrightSpark.ApproveCommentsTab.txb");
	}
	
	@QAFTestStep(description = "user click on submit button")
	public void userClickOnSubmitButton()
	{
//		waitForVisible("common.Submit.btn");
//		click("common.Submit.btn");
//		waitForNotVisible("login.loader.loc", 100);
	
		QAFExtendedWebElement SubmitBtn = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("common.Submit.btn")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", SubmitBtn);
		waitForNotVisible("login.loader.loc", 100);
	}
	
	@QAFTestStep(description = "user verify the BackTab")
	public void userVerifyTheBackTab()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("common.Back.btn");
		verifyPresent("common.Back.btn");
	}
	
	@QAFTestStep(description = "user verify BrightSparklist")
	public void userVerifyBrightSparklist()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("common.Nominee.label", 20);
		verifyPresent("common.Nominee.label");
		
		waitForVisible("common.Project.label", 20);
		verifyPresent("common.Project.label");
		
		waitForVisible("common.RewardName.label", 20);
		verifyPresent("common.RewardName.label");
		
		waitForVisible("common.PostedDate.label", 20);
		verifyPresent("common.PostedDate.label");
		
		waitForVisible("common.NominatedBy.label", 20);
		verifyPresent("common.NominatedBy.label");
		
		waitForVisible("common.Manager.label", 20);
		verifyPresent("common.Manager.label");
		
		waitForVisible("common.ManagerStatus.label", 20);
		verifyPresent("common.ManagerStatus.label");
		
		waitForVisible("common.HRStatus.label", 20);
		verifyPresent("common.HRStatus.label");
	}
	
	@QAFTestStep(description="user verify RewardCard")
	public void userVerifyRewardCard()
	{
		waitForVisible("BrightSpark.RewardCard.img", 20);
		verifyPresent("BrightSpark.RewardCard.img");
	}
	
	@QAFTestStep(description="user verify ValueAdditionslist")
	public void userVerifyValueAdditionslist()
	{
		waitForVisible("common.Challenging_situations_faced.label", 20);
		verifyPresent("common.Challenging_situations_faced.label");
		
		waitForVisible("common.Solutions_provided.label", 20);
		verifyPresent("common.Solutions_provided.label");
		
		waitForVisible("common.Benefits_accrued.label", 20);
		verifyPresent("common.Benefits_accrued.label");
		
		waitForVisible("common.Rationale_For_Nomination.label", 20);
		verifyPresent("common.Rationale_For_Nomination.label");
		
	}
	
	@QAFTestStep(description="user verify the Approve Reject and BackTabs")
	public void userVerifyTheApproveRejectAndBackTabs()
	{
		waitForNotVisible("login.loader.loc", 100);
		verifyPresent("common.Approve.btn");
		verifyPresent("common.Reject.btn");
		verifyPresent("common.Back.btn");
	}
	
	@QAFTestStep(description="user should verify MyNomination Page Title")
	public void userShouldVerifyMyNominationPageTitle()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("BrightSpark.MyNominationTitle.tle");
		verifyPresent("BrightSpark.MyNominationTitle.tle");
	}
	
	@QAFTestStep(description="user should verify BreadCrum Heading")
	public void userShouldVerifyBreadCrumHeading()
	{
		waitForVisible("BrightSpark.Bread_crum_heading1.lable");
		verifyPresent("BrightSpark.Bread_crum_heading1.lable");
	}
	
	@QAFTestStep(description="user should verify Page lables")
	public void userShouldVerifyPagelables()
	{
		waitForVisible("BrightSpark.MyNomination_Nominee.lable");
		verifyPresent("BrightSpark.MyNomination_Nominee.lable");
		
		waitForVisible("BrightSpark.MyNomination_OfReward.lable");
		verifyPresent("BrightSpark.MyNomination_OfReward.lable");
		
		waitForVisible("BrightSpark.MyNomination_PostedDate.lable");
		verifyPresent("BrightSpark.MyNomination_PostedDate.lable");
		
		waitForVisible("BrightSpark.MyNomination_ManagerStatus.lable");
		verifyPresent("BrightSpark.MyNomination_ManagerStatus.lable");
		
		waitForVisible("BrightSpark.MyNominationHRStatus.lable");
		verifyPresent("BrightSpark.MyNominationHRStatus.lable");
				
	}
	
	@QAFTestStep(description="user should verify Pagination")
	public void userShouldVerifyPagination()
	{
		waitForVisible("BrightSpark.Pagination.lable");
		verifyPresent("BrightSpark.Pagination.lable");
	}
	
}
   
