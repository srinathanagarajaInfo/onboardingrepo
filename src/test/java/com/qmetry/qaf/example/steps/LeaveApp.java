
package com.qmetry.qaf.example.steps;
import static com.qmetry.qaf.automation.step.CommonStep.*;
import static com.qmetry.qaf.automation.step.CommonStep.clear;
import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class LeaveApp extends WebDriverBaseTestPage<WebDriverTestPage>
{

	public static JavascriptExecutor js;
	public static Actions action;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}

	@Override
	public boolean isPageActive(PageLocator loc, Object... args) {

		return false;
	}

	@QAFTestStep(description="user click on LeaveMenuBar and ApplyLeave")
	public void userClickOnLeaveMenuBarAndApplyLeave()
	{

			waitForNotVisible("login.loader.loc", 100);
			QAFExtendedWebElement LeaveTab = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Leave.LeaveTab.lnk")));		
			action = new Actions(driver);
			action.moveToElement(LeaveTab).click().build().perform();
			
			waitForNotVisible("login.loader.loc", 100);
			QAFExtendedWebElement ApplyLeaveTab = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Leave.ApplyLeaveTab.lnk")));		
			action = new Actions(driver);
			action.moveToElement(ApplyLeaveTab).click().build().perform();
	
	}

	@QAFTestStep(description="user should select the FromAndToDate")
	public void userShouldSelectTheFromAndToDate()
	{
	
		waitForNotVisible("login.loader.loc", 50);
		clear("common.From.dte");
		sendKeys("13-August-2018", "common.From.dte");
		
     	waitForNotVisible("login.loader.loc", 50);
		clear("common.To.dte");
		sendKeys("15-August-2018", "common.To.dte");
		
	}
	
	@QAFTestStep(description="user should select the Reason for Leave")
	public void userShouldSelectTheReasonForLeave()
	{
		
		waitForNotVisible("login.loader.loc", 50);
		QAFExtendedWebElement SelectDropdown = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Leave.SelectDropdown.dpn")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", SelectDropdown);
		
		
		waitForNotVisible("login.loader.loc", 50);
		QAFExtendedWebElement selectTheReasonForLeave = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Leave.selectTheReasonForLeave.btn")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", selectTheReasonForLeave);
		
	}
	
	@QAFTestStep(description="user should select the Reason for Leave {0}")
	public void userShouldSelectTheReasonForLeave(String ReasonForLeave)
	{
		
		waitForNotVisible("login.loader.loc", 50);
		QAFExtendedWebElement SelectDropdown = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Leave.SelectDropdown.dpn")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", SelectDropdown);
		
		
		waitForNotVisible("login.loader.loc", 50);
		QAFExtendedWebElement selectTheReasonForLeave = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Leave.selectTheReasonForLeave.btn"), ReasonForLeave));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", selectTheReasonForLeave);
		
	}
	
	@QAFTestStep(description="user should select day")
	public void userShouldSelectTheDay()
	{
		waitForNotVisible("login.loader.loc", 50);
		click("Leave.SelectFullDay.btn");
		driver.findElement("Leave.SelectFullDay.btn").isSelected();
//		QAFExtendedWebElement SelectFullDay = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Leave.SelectFullDay.btn")));		
//		((JavascriptExecutor)driver).executeScript("arguments[0].click();", SelectFullDay);
	}

	@QAFTestStep(description="user click on apply button")
	public void userClickOnApplyButton()
	{
		waitForNotVisible("login.loader.loc", 50);
		click("common.Apply.btn");
	}
	
	@QAFTestStep(description="user click on LeaveAndTeamLeaveList")
	public void userClickOnLeaveAndTeamLeaveList()
	{	
		
		waitForNotVisible("login.loader.loc", 100);
		QAFExtendedWebElement MgrLeaveTab = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Leave.MgrLeaveTab.lnk")));		
//		((JavascriptExecutor)driver).executeScript("arguments[0].click();", MgrLeaveTab);
		action = new Actions(driver);
		action.moveToElement(MgrLeaveTab).click().build().perform();
		
		waitForNotVisible("login.loader.loc", 100);
		QAFExtendedWebElement MgrTeamLeaveListTab = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Leave.MgrTeamLeaveListTab.lnk")));		
//		((JavascriptExecutor)driver).executeScript("arguments[0].click();", MgrTeamLeaveListTab);
		action = new Actions(driver);
		action.moveToElement(MgrTeamLeaveListTab).click().build().perform();
	}
	
	@QAFTestStep(description="user should verify the Employee ID")
	public void userShouldVerifyTheEmployeeID()
	{
	
		waitForNotVisible("login.loader.loc", 50);
		waitForVisible("Leave.EmployeeID.ele", 20);
		verifyPresent("Leave.EmployeeID.ele");	
	}
	
	@QAFTestStep(description="user should verify the Employee Name")
	public void userShouldVerifyTheEmployeeName()
	{
		boolean EmployeeName= driver.findElement("Leave.EmployeeID.ele").verifyPresent();
		
	}
	@QAFTestStep(description="user should verify the Applied Date")
	public void userShouldVerifyTheAppliedDate()
	{
		boolean AppliedDate= driver.findElement("Leave.AppliedDate.ele").verifyPresent();
	}
	@QAFTestStep(description="user should verify the Employee Type")
	public void userShouldVerifyTheEmployeeType()
	{
		boolean Type= driver.findElement("Leave.Type.ele").verifyPresent();
	}
	@QAFTestStep(description="user should verify the Leave Duration")
	public void userShouldVerifyTheLeaveDuration()
	{
		boolean LeaveDuration= driver.findElement("Leave.LeaveDuration.ele").verifyPresent();
	}
	@QAFTestStep(description="user should verify the Leave Date")
	public void userShouldVerifyTheLeaveDate()
	{
		boolean LeaveDate= driver.findElement("Leave.LeaveDate.ele").verifyPresent();
	}
	@QAFTestStep(description="user should verify the Status")
	public void userShouldVerifyTheLeaveStatus()
	{
		boolean Status= driver.findElement("Leave.Status.ele").verifyPresent();
	}
	@QAFTestStep(description="user should verify the Leave Reason")
	public void userShouldVerifyTheLeaveReason()
	{
		boolean LeaveReason= driver.findElement("Leave.LeaveReason.ele").verifyPresent();
	}
	@QAFTestStep(description="user should verify the Project")
	public void userShouldVerifyTheProject()
	{
		boolean Project= driver.findElement("Leave.Project.ele").verifyPresent();
	}
	@QAFTestStep(description="user should verify the Managers comment")
	public void userShouldVerifyTheManagersComment()
	{
		boolean ManagerComment= driver.findElement("Leave.ManagerComment.ele").verifyPresent();
	}
	@QAFTestStep(description="user should verify the Actions")
	public void userShouldVerifyTheActions()
	{
		boolean Actions= driver.findElement("Leave.Actions.ele").verifyPresent();
	}
	@QAFTestStep(description="user should verify the Back dated Leave")
	public void userShouldVerifyTheBackDatedLeave()
	{
		boolean BacDatedLeave= driver.findElement("Leave.BacDatedLeave.ele").verifyPresent();
	}
	@QAFTestStep(description="user should verify the Submit and ApproveAll button")
	public void userShouldVerifyTheSubmitAndApproveAllButton()
	{
		boolean ApproveAllButton= driver.findElement("Leave.ApproveAllButton.ele").verifyPresent();
	}


}

