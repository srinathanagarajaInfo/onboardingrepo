/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;
import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.mouseOver;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;
import org.openqa.selenium.JavascriptExecutor;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.thoughtworks.selenium.webdriven.commands.Click;

public class Login extends WebDriverBaseTestPage<WebDriverTestPage>
{
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
		
	}

	@Override
	public boolean isPageActive(PageLocator loc, Object... args) {

		return false;
	}

	@QAFTestStep(description="user given username as {0} and password as {1}")
	public void userGivenUsernameAsAndPasswordAs(String username, String password) {

		waitForNotVisible("login.loader.loc", 100);
		sendKeys(username, "login.username.txb");
		sendKeys(password, "login.password.txb");
		click("login.loginButton.btn");
	}

	@QAFTestStep(description="Wait For Loader")
	public void WaitForLoader()
	{
		waitForNotVisible("login.loader.loc", 100);
	}

	@QAFTestStep(description="user should verify username is present in HomePage")  	
	public void userShouldVerifyUsernameIsPresentInHomePage()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("login.HomePage.logo", 20);
		verifyPresent("login.HomePage.logo");
			
	}

	@QAFTestStep(description="user Validate LogoutButton")
	public void userValidateLogoutButton()
	{
		
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("login.logoutButton.btn", 100);
		mouseOver("login.logoutButton.btn");
		QAFExtendedWebElement logoutButton = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("login.logoutButton.btn")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", logoutButton);
	}

	@QAFTestStep(description="user should verify UserNameRequiredErrorMessage is present")	
	public void userShouldVerifyUserNameRequiredErrorMessageIsPresent()
	{
		waitForVisible("login.UserNameRequired.txt");
		verifyPresent("login.UserNameRequired.txt");
	}
	
	
	@QAFTestStep(description="user should verify LoginButton is present")	
	public void userShouldVerifyLoginButtonIsPresent()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("login.loginButton.btn", 30);
		verifyPresent("login.loginButton.btn");
	}

}
   
	


