/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;


import static com.qmetry.qaf.automation.step.CommonStep.*;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;
import java.io.IOException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class ExpenseReimbursement extends WebDriverBaseTestPage<WebDriverTestPage>
{
	public static JavascriptExecutor js;
	public static Actions action;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}

	@Override
	public boolean isPageActive(PageLocator loc, Object... args) {

		return false;
	}

	@QAFTestStep(description = "user click on ManagerReimbursementTab {0}")
	public void userClickOnManagerReimbursementTab(String ManagerReimbursement)
	{
		waitForNotVisible("login.loader.loc", 100);
		QAFExtendedWebElement Reimbursement = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Expense.ManagerReimbursementTab.lnk"), ManagerReimbursement));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", Reimbursement);

	}
	

	@QAFTestStep(description = "user click on ManagerReimbursementTab and click on TeamReimbursementList")
	public void userClickOnManagerReimbursementTabAndClickOnTeamReimbursementList()
	{
		waitForNotVisible("login.loader.loc", 100);
		QAFExtendedWebElement ManagerReimbursement = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Expense.ManagerReimbursementTab.lnk")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", ManagerReimbursement);

		waitForNotVisible("login.loader.loc", 100);
		QAFExtendedWebElement TeamReimbursementList = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Expense.TeamReimbursementList.lnk")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", TeamReimbursementList);
//		action = new Actions(driver);
//		action.moveToElement(TeamReimbursementList).click().build().perform();

	}
	

	@QAFTestStep(description = "user click on My Expense List")
	public void userClickOnMyExpenseList() throws IOException
	{

		waitForNotVisible("login.loader.loc", 100);
		QAFExtendedWebElement MyExpenseListTab = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Expense.MyExpenseListTab.lnk")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", MyExpenseListTab);

	}

	@QAFTestStep(description = "user click on New Expense Button")
	public void userClickOnNewExpenseButton()
	{
		waitForNotVisible("login.loader.loc", 50);
		QAFExtendedWebElement NewExpenseTab = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Expense.NewExpenseTab.btn")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", NewExpenseTab);

	}

	@QAFTestStep(description = "user click on TeamReimbursementList")
	public void userClickOnTeamReimbursementList()
	{
		waitForNotVisible("login.loader.loc", 100);
		QAFExtendedWebElement TeamReimbursementList = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Expense.ReimbursementTeamList.lnk")));		
//		((JavascriptExecutor)driver).executeScript("arguments[0].click();", TeamReimbursementList);
		action = new Actions(driver);
		action.moveToElement(TeamReimbursementList).click().build().perform();

	}

	@QAFTestStep(description = "user fills all required fields as {0} {1} {2} {3}")
	public void userFillsAllRequiredFieldsAs(String ExpenseTitle, String ExpenseItemProject,String ExpenseItemCategory, String ExpenseAmount)
	{
		//
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("Expense.Title.txb", 50);
		sendKeys(ExpenseTitle, "Expense.Title.txb");
		//		########### Select Project

		waitForVisible("Expense.ExpenseItemProject.txb", 30);
		sendKeys(ExpenseItemProject, "Expense.ExpenseItemProject.txb");
		action = new Actions(driver);
		action.sendKeys(Keys.RETURN).perform();

		//		Select  Expense ######
		waitForVisible("Expense.ExpenseItemCategory.txb", 30);
		sendKeys(ExpenseItemCategory, "Expense.ExpenseItemCategory.txb");
		action = new Actions(driver);
		action.sendKeys(Keys.RETURN).perform();

		//		Select ExpenseAmountTextTab
		waitForVisible("Expense.AmountExpense.txb", 30);
		sendKeys(ExpenseAmount, "Expense.AmountExpense.txb");	
	}

	@QAFTestStep(description = "user click on PendingReimbursementRequest")
	public void userClickOnPendingReimbursementRequest()
	{
		waitForNotVisible("login.loader.loc", 100);
		QAFExtendedWebElement PendingReimbursementRequest = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Expense.TitlePendingReimbursementRequest.lnk")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", PendingReimbursementRequest);
	}

	@QAFTestStep(description = "user pass text in ExpensePendingformTextBox {0}")
	public void userPassTextInExpensePendingformTextBox(String ExpenseText)
	{
		waitForNotVisible("login.loader.loc", 100);
		QAFExtendedWebElement ExpensePendingformTextBox = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Expense.ExpensePendingformTextBox.txb"), ExpenseText));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", ExpensePendingformTextBox);

	}

	@QAFTestStep(description = "user should verify the ErrorRequiredField")
	public void userShouldVerifyTheErrorRequiredField()
	{
		waitForNotVisible("login.loader.loc", 100);
		verifyPresent("common.ErrorRequiredField.err");
	}
	
	@QAFTestStep(description="user click on AttachBrowseButton")
	public void userClickOnAttachBrowseButton() throws Exception
	{

		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("common.BrowseAttachment.atmn");
		click("common.BrowseAttachment.atmn");
	    QAFTestBase.pause(3000);
		Runtime.getRuntime().exec("C:\\Users\\rahil.saxena\\Desktop\\AutoIT\\FileUpload.exe");
		
	}

	
	
}
   