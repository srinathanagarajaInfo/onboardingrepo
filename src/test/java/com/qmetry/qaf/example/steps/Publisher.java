package com.qmetry.qaf.example.steps;

import static com.qmetry.qaf.automation.step.CommonStep.*;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class Publisher extends WebDriverBaseTestPage<WebDriverTestPage>{
			
    public static Actions action;
	
	@QAFTestStep(description="user click on Publisher and My Posts")
	public void userClickOnPublisherAndMyPosts()
	{
		
		waitForNotVisible("login.loader.loc", 100);
		QAFExtendedWebElement PublisherTab = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Publisher.MgrPublisherTab.lnk")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", PublisherTab);
		
		waitForNotVisible("login.loader.loc", 100);
		QAFExtendedWebElement MyPostsTab = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("Publisher.MgrMyPostsTab.lnk")));		
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", MyPostsTab);
		
	}
	
	@QAFTestStep(description="user should verify the Home Publisher locator")
	public void userShouldVerifyTheHomePublisherLocator()
	{
		waitForNotVisible("login.loader.loc", 100);
		verifyPresent("Publisher.HomePublisherLocator.ele");
	}
	
	@QAFTestStep(description="user should verify the FromAndToDate lable")
	public void userShouldVerifyTheFromAndToDateLable()
	{
		
		verifyPresent("Publisher.FromDatePicker.ele");
		verifyPresent("Publisher.ToDatePicker.ele");
	}
	
	@QAFTestStep(description="user should verify the KeyWord Textfield")
	public void userShouldVerifyTheKeyWordTextfield()
	{
	   verifyPresent("Publisher.KeywordTextFields.ele");
	}
	
	@QAFTestStep(description="user should verify the Category dropdown")
	public void userShouldVerifyTheCategoryDropdown()
	{
		verifyPresent("Publisher.CategoryDropdown.ele");
	}
	
	@QAFTestStep(description="user should verify the SearchAndReset buttons")
	public void userShouldVerifyTheSearchAndResetButtons()
	{
		verifyPresent("Publisher.SearchButton.ele");
		verifyPresent("Publisher.ResetButton.ele");		
	}
	
	@QAFTestStep(description="user should verify the column details")
	public void userShouldVerifyTheColumnDetails()
	{
		verifyPresent("Publisher.Title.lbl");
		verifyPresent("Publisher.Category.lbl");
		verifyPresent("Publisher.PostedOn.lbl");
		verifyPresent("Publisher.Action.lbl");

	}
	
	@QAFTestStep(description="user should verify the filter icon")
	public void userShouldVerifyTheFilterIcon()
	{	
	 verifyPresent("Publisher.FilterIcon.icon");	
	}
	
	@QAFTestStep(description="user should verify the Pagination lable")
	public void userShouldVerifyThePaginationLable()
	{
		verifyPresent("Publisher.PaginationLable.lbl");
	}
	
	@QAFTestStep(description="user should verify the EditAndDelete icons")
	public void userShouldVerifyTheEditAndDeleteIcons()
	{
		verifyPresent("Publisher.EditButton.btn");
		verifyPresent("Publisher.DeleteButton.btn");
	}
	
	@QAFTestStep(description="user should verify the Create Post button")
	public void userShouldVerifyTheCreatePostButton()
	{
	  verifyPresent("Publisher.CreatPost.btn");
	}
	
	///////////
	
	@QAFTestStep(description="user click on Create Button")
	public void userClickOnCreateButton()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("Publisher.CreatPost.btn");
		click("Publisher.CreatPost.btn");
		
	}
	
	@QAFTestStep(description="user should verify titlePostUrlImageUrl")
	public void userShouldVerifyTitlePostUrlImageUrl()
	{
	   
	   waitForNotVisible("login.loader.loc", 100);
	   verifyPresent("Publisher.loginTitle.lbl");
	   verifyPresent("Publisher.loginPostUrl.lbl");
	   verifyPresent("Publisher.loginImageUrl.lbl");
	}
	
	@QAFTestStep(description="user should verify select Locationdropdown and Category dropdown")
	public void userShouldVerifySelectLocationdropdowAndCategoryDropdown()
	{   
	   verifyPresent("Publisher.SelectLocation.dpn");
	   verifyPresent("Publisher.SelectCategory.dpn");
	}
	
	@QAFTestStep(description="user should verify Description")
	public void userShouldVerifyJourneyFromAndTo()
	{
		verifyPresent("Publisher.Description.txb");		
	}
	
	@QAFTestStep(description="user should verify submit button")
	public void userShouldVerifySubmitButton()
	{
	verifyPresent("common.Submit.btn");
	}
	
	@QAFTestStep(description="user should verify Back button")
	public void userShouldVerifyBackButton()
	{
	verifyPresent("common.Back.btn");
	}
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
