/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;
import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.mouseOver;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

public class Travel extends WebDriverBaseTestPage<WebDriverTestPage>
{
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}

	@Override
	public boolean isPageActive(PageLocator loc, Object... args) {

		return false;
	}

	@QAFTestStep(description="user click on managerView tab")
	public void userClickOnManagerViewTab()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("login.ManagerViewTab.loc", 50);
		click("login.ManagerViewTab.loc");
	}
	
	@QAFTestStep(description="user click on TravelRequestHeader tab")
	public void userClickOnTravelRequestHeaderTab()
	{
		waitForNotVisible("login.loader.loc", 100);
		waitForVisible("Travel.TravelRequestHeader.lnk");
		click("Travel.TravelRequestHeader.lnk");
	}
	
	@QAFTestStep(description="user click on ViewIcon")
	public void userClickOnViewIcon()
	{
		waitForNotVisible("login.loader.loc", 100);
		mouseOver(ConfigurationManager.getBundle().getString("Travel.ViewIcon.lnk"));
		click(ConfigurationManager.getBundle().getString("Travel.ViewIcon.lnk"));		
	}

	@QAFTestStep(description="user should verify Approve Reject and Back buttons")
	public void userShouldVerifyApproveRejectAndBackButtons()
	{
		waitForNotVisible("login.loader.loc", 100);
		verifyPresent("Travel.Approve.btn");
		verifyPresent("Travel.Reject.btn");
		verifyPresent("Travel.Back.btn");
	}
	
	@QAFTestStep(description="user click on ViewAll Button")
	public void userClickOnViewAllButton()
	{
		waitForNotVisible("login.loader.loc", 100);
		mouseOver("Travel.ViewAll.btn");
		click("Travel.ViewAll.btn");		
	}
	
	
}
   
	


